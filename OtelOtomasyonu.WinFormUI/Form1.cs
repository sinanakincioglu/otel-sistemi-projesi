﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OtelOtomasyonu.WinFormUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        BirimTipForm bt = new BirimTipForm();
        private void birimTipleriToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //birden fazla pencere açmasını engellemek için if...
            if (bt.IsDisposed)
                bt = new BirimTipForm();
            bt.MdiParent = this;
            bt.Show();
        }
        KasaForm kf = new KasaForm();

        private void kasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (kf.IsDisposed)
                kf = new KasaForm();
            kf.MdiParent = this;
            kf.Show();
        }
        KategoriForm ktg = new KategoriForm();
        private void kategorilerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ktg.IsDisposed)
                ktg = new KategoriForm();
            ktg.MdiParent = this;
            ktg.Show();
        }
        UrunForm urn = new UrunForm();
        private void ürünlerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (urn.IsDisposed)
                urn = new UrunForm();
            urn.MdiParent = this;
            urn.Show();
        }
        OdaTurleriForm odt = new OdaTurleriForm();
        private void özelliklerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (odt.IsDisposed)
                odt = new OdaTurleriForm();
            odt.MdiParent = this;
            odt.Show();
        }
        OdalarForm of = new OdalarForm();
        private void odalarToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (of.IsDisposed)
                of = new OdalarForm();
            of.MdiParent = this;
            of.Show();
        }
        OzelliklerForm ozf = new OzelliklerForm();
        private void ozelliklerToolStripMenuItem2_Click(object sender, EventArgs e)
        {

            if (ozf.IsDisposed)
                ozf = new OzelliklerForm();
            ozf.MdiParent = this;
            ozf.Show();
        }
        OdaOzellikForm ozellikForm = new OdaOzellikForm();
        private void OdaOzellikleriToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (ozellikForm.IsDisposed)
                ozellikForm = new OdaOzellikForm();
            ozellikForm.MdiParent = this;
            ozellikForm.Show();
        }
        MusteriForm mf = new MusteriForm();
        private void müşterilerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mf.IsDisposed)
                mf = new MusteriForm();
            mf.MdiParent = this;
            mf.Show();
        }
        PersonellerForm pf = new PersonellerForm();
        private void personellerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pf.IsDisposed)
                pf = new PersonellerForm();
            pf.MdiParent = this;
            pf.Show();
        }
        SatisForm sf = new SatisForm();
        private void satışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sf.IsDisposed)
            {
                sf = new SatisForm();
            }
            sf.MdiParent = this;
            sf.Show();
        }
    }
}
