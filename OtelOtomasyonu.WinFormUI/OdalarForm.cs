﻿using OtelOtomasyonu.ORM.Entity;
using OtelOtomasyonu.ORM.Facade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OtelOtomasyonu.WinFormUI
{
    public partial class OdalarForm : Form
    {
        public OdalarForm()
        {
            InitializeComponent();
        }

        private void OdalarForm_Load(object sender, EventArgs e)
        {
            Listele();
            cmbTuru.DataSource = otOrm.Select();
            cmbTuru.DisplayMember = "Adi";
            cmbTuru.ValueMember = "Id";
        }
        OdalarORM oOrm = new OdalarORM();
        OdaTurleriORM otOrm = new OdaTurleriORM();
        void Listele()
        {
            dataGridView1.DataSource = oOrm.Select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Odalar o = new Odalar();
            o.Aciklama = txtAciklama.Text;
            o.Adi = txtAdi.Text;
            o.OdaTurID = (int)cmbTuru.SelectedValue;
            bool sonuc = oOrm.Insert(o);
            if (sonuc)
            {
                Listele();
                MessageBox.Show("Oda eklenmiştir.");
            }
            else
                MessageBox.Show("Oda eklenirken hata oluştu!");
            txtAdi.Clear();
            txtAciklama.Clear();
        }
    }
}
