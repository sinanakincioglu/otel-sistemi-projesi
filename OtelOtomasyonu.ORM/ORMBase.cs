﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OtelOtomasyonu.ORM
{
    public class ORMBase<T> : IORM<T> where T:class
    {
        private string ClassName
        {
            get
            {
                //typeof:Tipi belli olmayan(generic bir) elemanın tipini belirlemeyi sağlar.
                return typeof(T).Name;
                //Name özelliği T tipinin (sınıfının) adını verir.
            }
        }
        public bool Delete(T entity)
        {
            SqlCommand cmd = new SqlCommand(string.Format("prc_{0}_Delete", ClassName), Tools.Baglanti);
            cmd.CommandType = CommandType.StoredProcedure;
            Tools.ParametreOlustur<T>(cmd, KomutTip.Delete, entity);
            return Tools.Exec(cmd);
        }

        public bool Insert(T entity)
        {
            SqlCommand cmd = new SqlCommand(string.Format("prc_{0}_Insert",ClassName), Tools.Baglanti);
            cmd.CommandType = CommandType.StoredProcedure;
            //GetProperties:T elemanı içerisindeki property'leri bir dizi olarak dönderen metottur.
            Tools.ParametreOlustur<T>(cmd, KomutTip.Insert, entity);
            //Üst satır aşağıdakilerlele aynı işi yapıyor.
            //PropertyInfo[] propertys = typeof(T).GetProperties();
            //foreach (PropertyInfo pi in propertys)
            //{
            //    //pi.Name:Property'nin adını verir.
            //    string name = pi.Name;
            //    if (name.ToLower() == "id" || name.ToLower()=="ıd")
            //    {
            //        continue;
            //    }
            //    //GeValue programdan gelen değeri getiriyor.
            //    object value = pi.GetValue(entity);
            //    cmd.Parameters.AddWithValue("@" + name,value);
            //}
            return Tools.Exec(cmd);
        }
        public object InsertScalar(T entity)
        {
            SqlCommand cmd = new SqlCommand(string.Format("prc_{0}_Insert", ClassName), Tools.Baglanti);
            cmd.CommandType = CommandType.StoredProcedure;
            Tools.ParametreOlustur<T>(cmd, KomutTip.Insert, entity);
            return Tools.ExecScalar(cmd);
        }



        public DataTable Select()
        {
            SqlDataAdapter adp = new SqlDataAdapter(string.Format("prc_{0}_Select", ClassName), Tools.Baglanti);
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }

        public bool Update(T entity)
        {
            SqlCommand cmd = new SqlCommand(string.Format("prc_{0}_Update", ClassName), Tools.Baglanti);
            cmd.CommandType = CommandType.StoredProcedure;
            Tools.ParametreOlustur<T>(cmd, KomutTip.Update, entity);
            return Tools.Exec(cmd);

        }
    }
}
